ARG PYTHON_IMG_TAG

FROM python:${PYTHON_IMG_TAG}
ARG PYTHON_IMG_TAG
ARG MAINTAINER
ARG HTTP_PROXY
ARG HTTPS_PROXY
# CGG Proxy & Cert + Python ENV Vars
COPY --from=cggsatmap/debian:buster \
    /etc/ssl/certs/ca-certificates.crt \
    /etc/ssl/certs/ca-certificates.crt
# Python faulthandler for base level C code
ENV http_proxy=${HTTP_PROXY} \
    https_proxy=${HTTPS_PROXY} \
    PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONFAULTHANDLER=1
LABEL "PYTHONDONTWRITEBYTECODE"="True" \
      "PYTHONUNBUFFERED"="True" \
      "PYTHONFAULTHANDLER"="True" \
      "PYTHON_IMG_TAG"="${PYTHON_IMG_TAG}" \
      "MAINTAINER"="${MAINTAINER}"

# Install runtime packages
RUN set -ex \
    && RUN_DEPS=" \
    libpcre3 \
    mime-support \
    postgresql-client \
    libglib2.0-0 \
    libgdal20 \
    libspatialindex-dev \
    " \
    && apt-get update && apt-get install -y --no-install-recommends $RUN_DEPS \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
COPY --from=cggsatmap/python:3.8 /opt/python/.venv /opt/python/.venv
ENV PATH="/opt/python/.venv/bin:$PATH"
# Compile deps to .pyc for speed gains
RUN python -c "import compileall; compileall.compile_path(maxlevels=10)"
# Create a group, user & dir to run our app
RUN groupadd -r appuser \
    && useradd --no-log-init -r -g appuser appuser \
    && mkdir -p /opt/app/static \
    && chown -R appuser:appuser /opt/app
WORKDIR /opt/app
COPY --chown=appuser:appuser . .
USER appuser:appuser
# Compile code to .pyc for speed gains
RUN python -m compileall /opt/app
# Add any environment variables needed by Django or your settings file here:
ENV DJANGO_SETTINGS_MODULE frontend.settings.production
# Gunicorn will listen on this port
EXPOSE 8000
# Create your docker-entrypoint.sh
ENTRYPOINT ["/opt/app/docker-entrypoint.sh"]
# Start Gunicorn - CMD appends to end of docker-entrypoint.sh as "$@"
# Request Line limit (4094) overriden - allows unlimited AJAX HTTP request length
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "--workers", "3", \
    "--worker-tmp-dir", "/dev/shm", "--threads", "6", \
    "--worker-class", "gthread", "--log-file", "-", \
    "--limit-request-line", "0", "frontend.wsgi"]
# Container build-specific labels
ARG GIT_COMMIT
LABEL "DJANGO_SETTINGS_MODULE"="frontend.settings.production" \
      "APP_USER"="appuser" \
      "GIT_COMMIT"="${GIT_COMMIT}"
