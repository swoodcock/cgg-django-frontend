from pathlib import Path

import setuptools

with open(Path(__file__).parent / ".env", "r") as env_file:
    env_vars = {}
    for line in env_file.readlines():
        if line.startswith("#") or not line.strip():
            continue
        key, value = line.strip().split("=", 1)
        env_vars[key] = value
    tag = env_vars["TAG"]
    tag_desc = env_vars["TAG_DESC"]

with open(Path(__file__).parent / "README.md", "r") as readme_file:
    long_description = readme_file.read()

setuptools.setup(
    name="frontend",
    version=tag,
    author="Sam Woodcock",
    author_email="<redacted>",
    description=tag_desc,
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="http://gitlab.<redacted>.com/services/frontend",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
    install_requires=[
        "django >= 3.0",
        "paramiko >= 2.6.0",
    ],
)
