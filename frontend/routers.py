class DjangoMetaRouter:

    django_db_tables = [
        "admin",
        "auth",
        "contenttypes",
        "sessions",
        "messages",
        "staticfiles",
        "migrations",
    ]

    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.django_db_tables:
            return "default"
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.django_db_tables:
            return "default"
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if (
            obj1._meta.app_label in self.django_db_tables
            or obj2._meta.app_label in self.django_db_tables
        ):
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label in self.django_db_tables:
            return db == "default"
        return None


class DBRouter:

    app_db_dict = {
        "airpol": "dbairpol",
        "sandman": "dbsandman",
        "wham": "dbwham02",
        "simpl": "dbsimpl",
        "offshore": "dboffshore",
    }

    def db_for_read(self, model, **hints):
        if model._meta.app_label in self.app_db_dict.keys():
            return self.app_db_dict[model._meta.app_label]
        return None

    def db_for_write(self, model, **hints):
        if model._meta.app_label in self.app_db_dict.keys():
            return self.app_db_dict[model._meta.app_label]
        return None

    def allow_relation(self, obj1, obj2, **hints):
        if (
            obj1._meta.app_label in self.app_db_dict.keys()
            or obj2._meta.app_label in self.app_db_dict.keys()
        ):
            return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        if app_label in self.app_db_dict.keys():
            return self.app_db_dict[app_label]
        return None
